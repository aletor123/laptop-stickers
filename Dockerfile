# pull official base image
FROM python:3.8.2-slim

# set work directory
WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install dependencies
#RUN apk add --no-cache jpeg-dev zlib-dev
#RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev
RUN apt-get update && apt-get install -y \
  binutils \
  gdal-bin \
  python3-gdal \
  libgdal-dev \
  libproj-dev
RUN pip install --upgrade pip
COPY ./requirements.txt /usr/src/app/requirements.txt
RUN pip install -r requirements.txt

# copy entrypoint.sh
COPY ./entrypoint.sh /usr/src/app/
# copy project
COPY . /usr/src/app/
RUN chmod +x entrypoint.sh
# run entrypoint.sh
ENTRYPOINT ["bash","/usr/src/app/entrypoint.sh"]