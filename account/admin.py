from django.contrib import admin

# Register your models here.
from account.models import Profile, KeysForSitesOwners

admin.site.register(Profile)
admin.site.register(KeysForSitesOwners)
