from django import forms
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _

from account.models import Profile


class UserRegistrationForm(forms.ModelForm):
    password = forms.CharField(label=_('Password'), widget=forms.PasswordInput)
    password2 = forms.CharField(label=_('Repeat password'), widget=forms.PasswordInput)
    site_owner = forms.BooleanField(label=_('I have my site with stickers'), required=False)
    site_url = forms.URLField(label=_('Url of your site'), required=False)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'email')
        labels = {
            'username': _('Username'),
            'first_name': _('First name'),
            'email': _('Email'),
        }

    def clean_password2(self):
        cd = self.cleaned_data
        if cd['password'] != cd['password2']:
            raise forms.ValidationError(_('Passwords don\'t match.'))
        return cd['password2']


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        exclude = ('user',)
        labels = {
            'site_name': _('Site name'),
            'site_label': _('Site Label'),
        }
