from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.db import models
from django.utils.translation import gettext_lazy as _


def sites_labels_directory_path(instance, filename):
    return 'sitesLabels/{0}.{1}'.format(instance.site_name, filename.split('.')[-1])


class Profile(models.Model):
    user = models.OneToOneField(User,
                                on_delete=models.CASCADE, related_name='profile')
    is_site_owner = models.BooleanField(blank=True, null=True)
    site_name = models.CharField(blank=True, null=True, max_length=20)
    site_url = models.URLField(verbose_name=_('Url of Your Site'), blank=True, null=True)
    site_label = models.ImageField(upload_to=sites_labels_directory_path, blank=True, null=True)
    permission = models.BooleanField(blank=True, null=True, verbose_name=_('Permission to use stickers'),
                                     help_text=_('Can common users use your stickers?'))

    def allowed_to_use_stickers(self):
        return self.permission

    def __str__(self):
        return 'Profile for user {}'.format(self.user.username)


class KeysForSitesOwners(models.Model):
    user = models.OneToOneField(User,
                                on_delete=models.CASCADE)
    key = models.CharField(max_length=10, unique=True, blank=True)
