from django.contrib.auth.models import User
from django.core.mail import send_mail

from laptopstickers import settings
from laptopstickers.celery import app


@app.task()
def new_site_owner(user_id, key):
    """Задача отправки email-уведомлений для подключения аккаунтов владельцов сайтов"""
    user = User.objects.get(id=user_id)
    subject = 'New stickers site owner {}'.format(user.username)
    message = 'Ура!\n\n {}, владелец сайта со стикерами, хочет подключиться к нашему сайту.\n' \
              'Вот ссылка на его сайт: {}\n\n' \
              'Если он вас устраивает перейдите по ссылке {}/account/{}/'.format(
        user.username, user.profile.site_url, settings.BASE_URL, key)
    mail_sent = send_mail(subject, message, settings.EMAIL_HOST_USER, ['andreyaletor2001@gmail.com'])
    return mail_sent


@app.task()
def send_message_to_new_site_owner(user_id):
    """Задача отправки email-уведомлений новым подключенным сайтам"""
    user = User.objects.get(id=user_id)
    subject = 'Вы подключены к сайту LaptopStickers'.format(user.username)
    message = 'Поздравляю, {}!\n\n' \
              'Теперь вы можете пользоваться нашим API и настроить свой аккаут' \
              '\n {}/profile/'.format(
        user.username, settings.BASE_URL)
    mail_sent = send_mail(subject, message, settings.EMAIL_HOST_USER, [user.email])
    return mail_sent
