from django.contrib.auth.models import User
from django.test import TestCase

from account.models import Profile
from account.tasks import send_message_to_new_site_owner, new_site_owner
from account.views import create_key, KEY_LENGTH


class TasksTestCase(TestCase):
    """
        Tests tasks and create_key function
    """

    @classmethod
    def setUpTestData(cls):
        cls.test_user = User.objects.create(username="test_user_1",
                                            first_name="fn_test",
                                            last_name="ln_test",
                                            email="test_email@test.ru")
        cls.test_user_profile = Profile.objects.create(user=cls.test_user, site_url="https://testsite.com")

    def test_create_key(self):
        self.key = create_key(self.test_user)
        self.assertEqual(len(self.key), KEY_LENGTH)

    def test_tasks(self):
        self.key = create_key(self.test_user)
        self.assertEqual(new_site_owner(self.test_user.id, self.key), 1)
        self.assertEqual(send_message_to_new_site_owner(self.test_user.id), 1)
