from http import HTTPStatus

from django.contrib.auth.models import User
from django.test import TestCase, Client

# Create your tests here.
from account.models import Profile
from account.views import create_key


class ViewsTestCase(TestCase):
    """
    Tests all account.views
    """

    def setUp(self):
        self.c = Client()
        self.superuser = User.objects.create_superuser('admin', 'admin@mail.ru')

    def test_register_create_site_owner(self):
        response = self.c.get("/account/register/")
        self.assertEqual(response.status_code, HTTPStatus.OK)
        response = self.c.post("/account/register/", {
            "username": "test_site_owner",
            "first_name": "test_first_name",
            "last_name": "test_last_name",
            "email": "testemail@test.ru",
            "password": "test_password",
            "password2": "test_password",
            "site_owner": "True",
            "site_url": "https://testsiteurl.com"
        })
        self.assertRedirects(response, '/account/login/')

    def test_register_create_common_user(self):
        response = self.c.get("/account/register/")
        self.assertEqual(response.status_code, HTTPStatus.OK)
        response = self.c.post("/account/register/", {
            "username": "test_common_user",
            "first_name": "test_first_name",
            "last_name": "test_last_name",
            "email": "testemail@test.ru",
            "password": "test_password",
            "password2": "test_password",
            "site_owner": "True",
            "site_url": "https://testsiteurl.com"
        })
        self.assertRedirects(response, '/account/login/')

    def test_append_site(self):
        test_site_owner = User.objects.create(username="test_site_owner",
                                              first_name="fn_test",
                                              last_name="ln_test",
                                              email="test_email@test.ru")
        test_site_owner_profile = Profile.objects.create(user=test_site_owner, site_url="https://testsite.com")
        key = create_key(test_site_owner)
        response = self.c.post('/account/login/', {'username': 'aletor',
                                                   'password': '123'})
        self.c.force_login(user=self.superuser)
        response = self.c.post('/account/{}/'.format(key))
        self.assertTemplateUsed(response, template_name="append_side_success.html")
