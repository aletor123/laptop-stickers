from django.urls import path
from django.contrib.auth import views as auth_views

from account.views import ProfileView
from account import views

app_name = 'account'

urlpatterns = [
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('register/', views.register, name='register'),
    path('<str:key>/', views.append_site, name='new_site_owner'),
    path('profile/', ProfileView, name='profile')
]
