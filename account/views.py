import random
import string

from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import Group
from django.db import IntegrityError
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404

from django.views.generic.edit import FormMixin

from account.forms import UserRegistrationForm, ProfileForm
from account.models import Profile, KeysForSitesOwners
from account.tasks import new_site_owner, send_message_to_new_site_owner

KEY_LENGTH = 10


def key_generator(size=KEY_LENGTH, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.SystemRandom().choice(chars) for _ in range(size))


def create_key(site_owner):
    object_for_key_save = KeysForSitesOwners.objects.create(user=site_owner)
    while True:
        try:
            object_for_key_save.key = key_generator()
            object_for_key_save.save()
            break
        except IntegrityError:
            continue
    return object_for_key_save.key


def register(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            cd = user_form.cleaned_data
            new_user = user_form.save(commit=False)
            new_user.set_password(cd['password'])
            new_user.save()
            Profile.objects.create(user=new_user, site_url=cd['site_url'])
            if cd['site_owner']:
                key = create_key(new_user)
                new_site_owner.delay(new_user.id, key)
            return HttpResponseRedirect('/account/login/')
    else:
        user_form = UserRegistrationForm()
        return render(request, 'registration/register.html', {'user_form': user_form})


class ProfileView(LoginRequiredMixin, FormMixin):
    template_name = '/profile.html'
    success_url = '/account/profile/'
    form_class = ProfileForm

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        context['user_profile'] = Profile.objects.get(user=self.request.test_user)
        return context


@user_passes_test(lambda u: u.is_superuser)
def append_site(request, key):
    key_obj = get_object_or_404(KeysForSitesOwners, key=key)
    user = key_obj.user
    send_message_to_new_site_owner.delay(user.id)
    group, _ = Group.objects.get_or_create(name='site_owners')
    profile = Profile.objects.get(user=user)
    profile.user.groups.add(group)
    profile.is_site_owner = True
    profile.save()
    key_obj.delete()
    return render(request, 'append_side_success.html', {'new_site_owner': user})

# TODO append_side success template, Profile template
