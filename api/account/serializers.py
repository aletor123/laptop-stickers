from django.contrib.auth.models import User
from rest_framework import serializers


class StickerSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'email', 'source_url']
