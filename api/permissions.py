from django.contrib.auth.models import User
from rest_framework.permissions import BasePermission


class IsSiteOwner(BasePermission):
    """
        Permission checks if the test_user is the owner of the site
    """

    def has_permission(self, request, view):
        user = User.objects.get(id=request.user.id)
        return user.groups.filter(name='site_owners').exists()
