let laptopStickersStyles = new Array(5)
laptopStickersStyles.push(
    'https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css',
    stickers_site_url +'/static/css/base_api.css',
    stickers_site_url +'/static/css/workArea.css',
    'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css',
    stickers_site_url +'/static/css/jquery.ui.rotatable.css',
)
let laptopStickersScripts = new Array(9)
laptopStickersScripts.push(
    'https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js',
    stickers_site_url +'/static/js/jquery.ui.rotatable.min.js',
    stickers_site_url +'/static/js/ajaxSetup.js',
    stickers_site_url +'/static/js/calculatingPosition.js',
    stickers_site_url +'/static/js/laptopChoosing.js',
    stickers_site_url +'/static/js/stickerChoosing.js',
    stickers_site_url +'/static/js/searchByTags.js',
    stickers_site_url +'/static/js/uploadingStickers.js',
    stickers_site_url +'/static/js/actionOnResize.js',
)

let jQ = false;

function initJQ() {
	if (typeof(jQuery) == 'undefined' || typeof(jQuery.ui) == 'undefined') {
		if (!jQ) {
			jQ = true;
			document.write('<scr' + 'ipt type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></scr' + 'ipt>');
			document.write('<scr' + 'ipt type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></scr' + 'ipt>');
		}
		setTimeout('initJQ()', 50);
	} else {
		(function($) {
		$(function() {
            for(let style in laptopStickersStyles){
                let link = document.createElement('link');
                link.href = laptopStickersStyles[style];
                link.rel = 'stylesheet';
                link.type = 'text/css';
                document.head.append(link);
            }
            for(let script in laptopStickersScripts){
                let script_el = document.createElement('script');
                script_el.src = laptopStickersScripts[script];
                document.body.append(script_el);
            }
		})
		})(jQuery)
	}
}
initJQ();