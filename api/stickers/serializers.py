from rest_framework import serializers
from taggit.models import Tag

from stickers.models import Sticker


class StringListField(serializers.ListField):
    child = serializers.CharField()

    def to_representation(self, data):
        return ' '.join(data.values_list('name', flat=True))


class StickerSerializer(serializers.ModelSerializer):
    tags = StringListField()
    new_tags = ''

    class Meta:
        model = Sticker
        exclude = ['owner']
        read_only_fields = ['id', 'owner']

    def create(self, validated_data):
        if 'tags' in validated_data:
            self.new_tags = validated_data.pop('tags')
        instance = super(StickerSerializer, self).create(validated_data)
        if self.new_tags:
            instance.tags.set(*self.new_tags)
        return instance

    def update(self, instance, validated_data):
        if 'tags' in validated_data:
            self.new_tags = validated_data.pop('tags')
        instance = super(StickerSerializer, self).update(instance, validated_data)
        if self.new_tags:
            instance.tags.set(*self.new_tags)
        return instance
