from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.decorators import action
from rest_framework.exceptions import APIException
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from api.permissions import IsSiteOwner
from api.stickers.serializers import StickerSerializer
from stickers.models import Sticker


class StickerViewSet(viewsets.ModelViewSet):
    serializer_class = StickerSerializer
    authentication_classes = [TokenAuthentication, SessionAuthentication]
    permission_classes = [IsAuthenticated, IsSiteOwner]

    @action(detail=False, methods=['get'], name=str)
    def find_for_name(self, request):
        name = request.GET['name']
        sticker = get_object_or_404(Sticker.objects.filter(owner=request.user), name=name)
        serializer = self.get_serializer(sticker)
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        if 'owner' in request.data:
            # Owner cant change sticker's owner
            raise APIException("You can't change sticker's owner. You are owner.", code=400)
        return super(StickerViewSet, self).update(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        return Sticker.objects.filter(owner=self.request.user)
