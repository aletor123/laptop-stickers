from django.contrib.auth.models import User, Group
from django.http import HttpRequest
from rest_framework import status
from rest_framework.test import APIClient, APITestCase
from rest_framework.authtoken.models import Token

from account.models import Profile

TEST_SERVER_HOST = 'testserver'


def generate_request():
    request = HttpRequest()
    request.method = "GET"
    request.META["HTTP_HOST"] = TEST_SERVER_HOST
    return request


class BasicAPITestCase(APITestCase):
    def response_is_ok(self, response):
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         'Expected Response Code 200, received {0} instead.'
                         .format(response.status_code))

    @classmethod
    def setUpTestData(cls):
        """
        SetUpData:
            api_client - APIclient
            test_user - user for tests
            test_user_profile - profile of test user
                | he is site owner that uses api for interaction with stickers
            request - request that we use to generate full url in serializer
            ----------------------------------------------------------------
            setUp Token AUTHORIZATION for test_user
        """
        cls.api_client = APIClient()
        cls.request = generate_request()
        cls.test_user = User.objects.create(username="test_user_1",
                                            first_name="fn_test",
                                            last_name="ln_test",
                                            email="testemail@test.ru")
        cls.test_user_profile = Profile.objects.create(user=cls.test_user,
                                                       site_url="https://testsite.com",
                                                       is_site_owner=True,
                                                       site_name='test_site_name')
        group = Group.objects.create(name='site_owners')
        cls.test_user.groups.add(group)
        token, created = Token.objects.get_or_create(user=cls.test_user)
        cls.api_client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
