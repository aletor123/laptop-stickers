import factory
import random

from stickers import models as sticker_model
from laptop import models as laptop_model

# STICKER SIZE CONSTS
MIN_STICKER_HEIGHT = 20
MAX_STICKER_HEIGHT = 120
MIN_STICKER_WIDTH = 20
MAX_STICKER_WIDTH = 120

# LAPTOP SIZE CONSTS
MIN_LAPTOP_HEIGHT = 150
MAX_LAPTOP_HEIGHT = 300
MIN_LAPTOP_WIDTH = 150
MAX_LAPTOP_WIDTH = 350

# Sticker tags consts
COUNT_OF_TAGS_FOR_ONE_STICKER = 3
COUNT_OF_TAGS = 10


# Laptops consts
COUNT_OF_MANUFACTURERS = 3


class RandomStickerFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = sticker_model.Sticker

    name = factory.Sequence(lambda x: 'some_sticker_name_{}'.format(x))
    height = factory.LazyAttribute(lambda x: random.randrange(MIN_STICKER_HEIGHT, MAX_STICKER_HEIGHT + 1))
    width = factory.LazyAttribute(lambda x: random.randrange(MIN_STICKER_WIDTH, MAX_STICKER_WIDTH + 1))
    source_url = factory.Faker('image_url')
    image = factory.django.ImageField(color='green')


class RandomStickerFactoryWithTags(RandomStickerFactory):
    @factory.post_generation
    def post_tags(self, create, extracted, **kwargs):
        tags = []
        for i in range(COUNT_OF_TAGS_FOR_ONE_STICKER):
            tags.append('tag_{}'.format(random.randrange(0, COUNT_OF_TAGS)))
        self.tags.add(*tags)


class RandomLaptopFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = laptop_model.Laptop

    manufacturer = factory.Sequence(lambda x: 'manufacturer_{}'.format(random.randrange(0, COUNT_OF_MANUFACTURERS)))
    model = factory.Sequence(lambda x: 'model_{}'.format(x))
    height = factory.LazyAttribute(lambda x: random.randrange(MIN_LAPTOP_HEIGHT, MAX_LAPTOP_HEIGHT + 1))
    width = factory.LazyAttribute(lambda x: random.randrange(MIN_LAPTOP_WIDTH, MAX_LAPTOP_WIDTH + 1))
    image = factory.django.ImageField(color='blue')
