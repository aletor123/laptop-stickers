import os
import random

from django.contrib.auth.models import User
from django.core.paginator import Paginator
from django.urls import reverse

from api.laptops.serializers import LaptopSerializer

from api.tests.base_test_case import BasicAPITestCase
from api.tests.factories.factories import RandomLaptopFactory, COUNT_OF_MANUFACTURERS, \
    RandomStickerFactoryWithTags, COUNT_OF_TAGS, COUNT_OF_TAGS_FOR_ONE_STICKER
from laptop.models import Laptop
from laptopstickers import settings
from stickers.models import Sticker

from workArea.views import STICKERS_AT_ONE_PAGE as S_A_O_P, get_context

from django.utils.translation import gettext_lazy as _

urls = {
    'get_token': reverse('api:get_token'),
    'choose_laptop_manufacturer': reverse('api:choose_laptop_manufacturer'),
    'search_by_tags': reverse('api:search_by_tags'),
    'choose_laptop': reverse('api:choose_laptop'),
    'upload_sticker': reverse('api:upload_sticker'),
    'work_area_api_view': reverse('api:work_area_api_view')
}

settings.LAPTOPS_DIR = os.path.join(settings.MEDIA_DIR, 'test_laptops')


class APIViewsWithLaptopsTest(BasicAPITestCase):
    COUNT_OF_LAPTOPS = 10

    def setUp(self):
        """SetUp Laptops for tests"""
        self.all_laptops = RandomLaptopFactory.create_batch(self.COUNT_OF_LAPTOPS)

    def doCleanups(self):
        """Delete test laptops folder"""
        Laptop.objects.all().delete()

    def test_work_area_view(self):
        response = self.api_client.get(urls['work_area_api_view'])
        stickers = Sticker.objects.all()[:S_A_O_P]
        laptop_form = get_context()
        self.response_is_ok(response)
        self.assertTemplateUsed(response, 'base_api_workArea.html',
                                "Expected base_api_workArea.html but it another was used")
        self.assertEqual(response.data['laptopForm'].fields['manufacturer'].choices,
                         laptop_form.fields['manufacturer'].choices,
                         "Manufacturer choices for laptop_form has broken")
        self.assertEqual(response.data['laptopForm'].fields['model'].choices,
                         laptop_form.fields['model'].choices,
                         "Model choices for laptop_form has broken")
        self.assertEqual(list(response.data['stickers']), list(stickers),
                         "QuerySet of stickers in context are not equals to normal")
        self.assertEqual(response.data['site_name'], self.test_user.profile.site_name,
                         "site_name in context are not equals to normal, expected {}\n"
                         "but it is {}".format(self.test_user.profile.site_name, response.data['site_name']))

    def test_choose_laptop_manufacturer(self):
        for i in range(COUNT_OF_MANUFACTURERS):
            manufacturer = "manufacturer_{}".format(i)
            with self.subTest(manufacturer=manufacturer):
                response = self.api_client.get(urls['choose_laptop_manufacturer'],
                                               {'manufacturer': manufacturer})
                laptops = Laptop.objects.filter(manufacturer=manufacturer)
                model_choices = {laptop.id: laptop.model for laptop in laptops}
                model_choices['none'] = _('--Choice model--')
                self.response_is_ok(response)
                self.assertEqual(response.data, model_choices,
                                 "View responses wrong model_choices")

    def test_choose_laptop(self):
        laptops = Laptop.objects.all()
        for laptop in laptops:
            with self.subTest(laptop=laptop):
                response = self.api_client.get(urls['choose_laptop'], {'id': laptop.id})
                serializer = LaptopSerializer(laptop, context={'request': self.request})
                self.response_is_ok(response)
                self.assertEqual(response.data, serializer.data,
                                 "View responses wrong laptop data")

    # def test_CustomAuthToken(self):
    #     response = self.api_client.get(urls['get_token'])
    #     self.request_is_ok(response)


class APIViewsWithStickersTest(BasicAPITestCase):
    """TestCase for API Views that use stickers in their responses"""
    COUNT_OF_STICKERS = 20

    def setUp(self):
        """SetUp stickers for tests"""
        self.all_stickers = RandomStickerFactoryWithTags.create_batch(self.COUNT_OF_STICKERS,
                                                                      owner=self.test_user)

    def doCleanups(self):
        """Delete test stickers and laptops folder"""
        Sticker.objects.all().delete()

    def _generate_common_user_with_stickers(self):
        """In some tests, for a full check, you need to add stickers from another user"""
        user_for_cur_test = User.objects.create(username='user_for_cur_test', first_name='some_name')
        RandomStickerFactoryWithTags.create_batch(self.COUNT_OF_STICKERS, owner=user_for_cur_test)

    def _test_search_by_tags(self, site_name=''):
        """
        This method is used by two tests:
            1.If we need to sent response from another site
            2.If we need to sent response from this site
        :param site_name: name of site with stickers
        """
        for i in range(COUNT_OF_TAGS_FOR_ONE_STICKER):
            with self.subTest(msg='Count of tags in one sticker = {}'.format(i)):
                tags = []
                while len(tags) <= i:
                    new_tag = 'tag_{}'.format(random.randrange(0, COUNT_OF_TAGS))
                    if new_tag not in tags:
                        tags.append(new_tag)
                tags_string = ','.join(tags)
                response = self.api_client.get(
                    urls['search_by_tags'],
                    {'site_name': site_name,
                     'tag': tags_string}
                )
                stickers = Sticker.objects.filter(tags__name__in=tags).distinct()
                stickers_to_compare = stickers[:S_A_O_P]
                stickers_ids = list(stickers.values_list('id', flat=True))
                self.assertEqual(response.data.get('stickers_ids'), stickers_ids,
                                 'List stickers_ids  in context are dont match')
                self.assertEqual(list(response.data.get('stickers')), list(stickers_to_compare),
                                 'QuerySet of stickers in context are not equals to normal')
                self.response_is_ok(response)

    def test_search_by_tags_from_another_site(self):
        self._test_search_by_tags(site_name=self.test_user.profile.site_name)

    def test_search_by_tags_from_this_site(self):
        self._generate_common_user_with_stickers()
        self._test_search_by_tags()

    def test_search_by_tags_without_tags(self):
        """If request does not contain tags, view must throw APIException"""
        response = self.api_client.get(urls['search_by_tags'])
        self.assertEqual(response.data.get('detail'), 'There is no tags in request',
                         "Expected: 'There is no tags in request' Exception")

    def _test_upload_stickers(self, site_name='', stickers_ids=None):
        """
        This method is used by two tests:
            1.If we need to sent response from another site
            2.If we need to sent response from this site
        :param site_name: name of site with stickers
        :param stickers_ids: id of stickers that were saved after search_by_tags
        """
        if stickers_ids:
            """
            if stickers_ids is not null
            then the situation of loading stickers after search_by_tags is tested
            """
            stickers = Sticker.objects.filter(id__in=stickers_ids)
        else:
            stickers = Sticker.objects.all()
        paginator = Paginator(stickers, S_A_O_P)
        for page in range(1, paginator.num_pages + 2):
            with self.subTest(msg='Count of pages = {}'.format(paginator.num_pages), page=page):
                data = {'page': page,
                        'site_name': site_name}
                if stickers_ids:
                    data['stickers_ids[]'] = stickers_ids
                if page == paginator.num_pages + 1:
                    """If view gets out of range page, it must throw APIException"""
                    response = self.api_client.get(urls['upload_sticker'], data)
                    self.assertEqual(response.data.get('detail'), 'This page is empty',
                                     "View doesn't sended APIException")
                    return 1
                response = self.api_client.get(urls['upload_sticker'], data)
                stickers = paginator.page(page)
                self.assertEqual(list(response.data.get('stickers')), list(stickers),
                                 "View responses wrong queryset of stickers")

    def test_upload_stickers_from_another_site(self):
        self._test_upload_stickers(site_name=self.test_user.profile.site_name)

    def test_upload_stickers_from_this_site(self):
        self._generate_common_user_with_stickers()
        self._test_upload_stickers()

    def test_upload_stickers_after_search_by_tags(self):
        stickers_ids = [i for i in range(1, self.COUNT_OF_STICKERS + 1)]
        self._test_upload_stickers(stickers_ids=stickers_ids)
