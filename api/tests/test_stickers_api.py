import tempfile

from PIL import Image
from rest_framework import status

from api.stickers.serializers import StickerSerializer
from api.tests.base_test_case import BasicAPITestCase
from api.tests.factories.factories import RandomStickerFactoryWithTags
from stickers.models import Sticker


class StickersAPITest(BasicAPITestCase):
    """ Test module for GET all stickers API """
    COUNT_OF_STICKERS = 20

    def setUp(self):
        """
        SetUp stickers for tests
        """
        self.all_stickers = RandomStickerFactoryWithTags.create_batch(self.COUNT_OF_STICKERS, owner=self.test_user)

    def doCleanups(self):
        """
        Delete test stickers folder
        """
        Sticker.objects.all().delete()

    def test_create_sticker(self):
        tmp_image = Image.new('RGB', (100, 100))
        image = tempfile.NamedTemporaryFile(suffix='.jpg')
        tmp_image.save(image)
        image.seek(0)
        sticker_params = {'name': 'test_sticker',
                          'width': 50,
                          'height': 75,
                          'image': image,
                          'tags': '1tag,2tag,3tag'}
        response = self.api_client.post('/api/stickers/', sticker_params, format='multipart')
        sticker = Sticker.objects.get(name='test_sticker')
        serializer = StickerSerializer(sticker, context={'request': self.request})
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED,
                         'Expected Response Code 201, received {0} instead.'
                         .format(response.status_code))

    def test_get_all_stickers(self):
        # get API response
        response = self.api_client.get('/api/stickers/')
        # get data from db
        stickers = Sticker.objects.filter(owner=self.test_user)
        serializer = StickerSerializer(stickers, many=True, context={'request': self.request})
        self.assertEqual(response.data, serializer.data)
        self.response_is_ok(response)

    def test_get_sticker_by_name(self):
        for sticker in self.all_stickers:
            with self.subTest(sticker=sticker):
                response = self.api_client.get('/api/stickers/find_for_name/?name={}'.format(sticker.name))
                serializer = StickerSerializer(sticker, context={'request': self.request})
                self.response_is_ok(response)
                self.assertEqual(response.data, serializer.data)

    def test_delete_all_stickers(self):
        # delete using API
        for sticker in self.all_stickers:
            with self.subTest(sticker__id=sticker.id):
                response = self.api_client.delete('/api/stickers/{}/'.format(sticker.id))
                self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT,
                                 'Expected Response Code 204, received {0} instead.'
                                 .format(response.status_code))
        # get data from db
        stickers = Sticker.objects.filter(owner=self.test_user)
        serializer = StickerSerializer(stickers, many=True, context={'request': self.request})
        self.assertEqual(serializer.data, [])

    # PATCH TESTS

    def test_change_sticker_name(self):
        ANOTHER_NAME = 'another_name'
        for sticker in self.all_stickers:
            with self.subTest(sticker__id=sticker.id):
                response = self.api_client.patch('/api/stickers/{}/'.format(sticker.id), {'name': ANOTHER_NAME + "_{}".format(sticker.id)})
                self.response_is_ok(response)
                check_sticker = Sticker.objects.get(id=sticker.id)
                self.assertEqual(check_sticker.name, ANOTHER_NAME + "_{}".format(sticker.id))

    def test_change_sticker_height(self):
        ANOTHER_HEIGHT = 95
        for sticker in self.all_stickers:
            with self.subTest(sticker__id=sticker.id):
                response = self.api_client.patch('/api/stickers/{}/'.format(sticker.id), {'height': ANOTHER_HEIGHT})
                self.response_is_ok(response)
                check_sticker = Sticker.objects.get(id=sticker.id)
                self.assertEqual(check_sticker.height, ANOTHER_HEIGHT)

    def test_change_sticker_width(self):
        ANOTHER_WIDTH = 85
        for sticker in self.all_stickers:
            with self.subTest(sticker__id=sticker.id):
                response = self.api_client.patch('/api/stickers/{}/'.format(sticker.id), {'width': ANOTHER_WIDTH})
                self.response_is_ok(response)
                check_sticker = Sticker.objects.get(id=sticker.id)
                self.assertEqual(check_sticker.width, ANOTHER_WIDTH)

    def test_change_sticker_image(self):
        # To send request we need to create a stream object
        tmp_image = Image.new('RGB', (100, 100))
        ANOTHER_IMAGE = tempfile.NamedTemporaryFile(suffix='.jpg')
        for sticker in self.all_stickers:
            with self.subTest(sticker__id=sticker.id):
                tmp_image.save(ANOTHER_IMAGE)
                ANOTHER_IMAGE.seek(0)
                response = self.api_client.patch('/api/stickers/{}/'.format(sticker.id), {'image': ANOTHER_IMAGE}, format="multipart")
                self.response_is_ok(response)

    def test_change_sticker_tags(self):
        ANOTHER_TAGS = ['first_tag', 'second_tag', 'third_tag']
        for sticker in self.all_stickers:
            with self.subTest(sticker__id=sticker.id):
                response = self.api_client.patch('/api/stickers/{}/'.format(sticker.id), {'tags': ANOTHER_TAGS})
                self.response_is_ok(response)
                check_sticker = Sticker.objects.get(id=sticker.id)
                new_sticker_tags = check_sticker.tags_list().sort()
                self.assertEqual(new_sticker_tags, ANOTHER_TAGS.sort())

    def test_change_sticker_source_url(self):
        ANOTHER_SOURCE_URL = 'https://stickerssite.com/some/new/sticker/url/'
        for sticker in self.all_stickers:
            with self.subTest(sticker__id=sticker.id):
                response = self.api_client.patch('/api/stickers/{}/'.format(sticker.id), {'source_url': ANOTHER_SOURCE_URL})
                self.response_is_ok(response)
                check_sticker = Sticker.objects.get(id=sticker.id)
                self.assertEqual(check_sticker.source_url, ANOTHER_SOURCE_URL)
