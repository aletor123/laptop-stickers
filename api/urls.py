from django.urls import path, include
from rest_framework.routers import DefaultRouter

from api import views
from api.stickers.views import StickerViewSet
from api.views import WorkAreaApiView, CustomAuthToken
from api.stickers import views as sticker_views

app_name = 'api'

router = DefaultRouter()
router.register('stickers', StickerViewSet, basename='sticker')

urlpatterns = [
    # Url for token
    path('getToken/', CustomAuthToken.as_view(), name='get_token'),
    # Urls for workArea
    path('workArea/', WorkAreaApiView.as_view(), name='work_area_api_view'),
    path('uploadSticker/', views.upload_stickers, name='upload_sticker'),
    path('chooseLaptop/', views.choose_laptop, name='choose_laptop'),
    path('searchByTags/', views.search_by_tags, name='search_by_tags'),
    path('chooseLaptopManufacturer/', views.choose_laptop_manufacturer, name='choose_laptop_manufacturer'),

    # Urls for stickers
    path('', include(router.urls)),
]
