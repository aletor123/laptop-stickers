from django.core.paginator import Paginator, EmptyPage
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.exceptions import APIException
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from django.utils.translation import gettext_lazy as _

from account.models import Profile
from api.laptops.serializers import LaptopSerializer
from api.permissions import IsSiteOwner
from laptop.models import Laptop
from workArea.views import get_context, STICKERS_AT_ONE_PAGE as S_A_O_P
from stickers.models import Sticker


class WorkAreaApiView(APIView):
    """
    APIView that allows site_owners to get workArea.
    """
    renderer_classes = [TemplateHTMLRenderer]
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated, IsSiteOwner]

    def get(self, request, *args, **kwargs):
        """
        :return: work area for site_owners with
        script that loads static and another scripts for work
        """
        laptop_form = get_context()
        site_owner = request.user
        stickers = Sticker.objects.filter(owner=site_owner)[:S_A_O_P]
        return Response({'laptopForm': laptop_form,
                         'stickers': stickers,
                         'site_name': site_owner.profile.site_name}, template_name='base_api_workArea.html')


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def choose_laptop_manufacturer(request):
    """
    Выбор производителя ноутбуков и изменение набора выбора моделей
    :return: словарь, где ключ=id модели, значение=названию модели
    """
    manufacturer = request.GET.get('manufacturer')
    if manufacturer != '0':
        laptops = Laptop.objects.filter(manufacturer=manufacturer)
    else:
        laptops = Laptop.objects.all()
    model_choices = {laptop.id: laptop.model for laptop in laptops}
    model_choices['none'] = _('--Choice model--')
    return Response(model_choices)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def choose_laptop(request):
    """
    Выбор ноутбука
    :return: {
                'image.url': ссылка на картинку ноутбука,
                'width': ширина ноутбука,
                'height': высота ноутбука,
             }
    """
    laptop = get_object_or_404(Laptop, id=request.GET.get('id'))
    serializer = LaptopSerializer(laptop, context={'request': request})
    return Response(serializer.data)


@api_view(['GET'])
@renderer_classes([TemplateHTMLRenderer])
def search_by_tags(request):
    """
    Поиск стикеров по тегу
    :return: Html текст
    """
    if request.GET.get('site_name'):
        site_owner = get_object_or_404(Profile, site_name=request.GET.get('site_name')).user
        stickers = Sticker.objects.filter(owner=site_owner)
    else:
        stickers = Sticker.objects.all()
    if request.GET.get('tag'):
        tags = request.GET.get('tag').split(',')
        for i, tag in enumerate(tags):
            tags[i] = tag.strip()
        stickers = stickers.filter(tags__name__in=tags).distinct()
        stickers_for_render = stickers[:S_A_O_P]
        stickers_ids = list(stickers.values_list('id', flat=True))
    else:
        raise APIException("There is no tags in request", code=404)
    return Response({'stickers': stickers_for_render, 'stickers_ids': stickers_ids},
                    template_name='stickers/stickers_for_placer_render.html')


@api_view(['GET'])
@renderer_classes([TemplateHTMLRenderer, JSONRenderer, ])
def upload_stickers(request):
    """
    Подгрузка стикеров при прокрутке
    :return: Html текст
    """
    page = request.GET['page']
    sticker_ids = request.GET.getlist('stickers_ids[]')
    if sticker_ids:
        """
        If search by tags is already used, it uses presaved stickers id
        """
        paginator = Paginator(Sticker.objects.filter(id__in=sticker_ids), S_A_O_P)
    else:
        if request.GET.get('site_name'):
            site_owner = get_object_or_404(Profile, site_name=request.GET['site_name']).user
            paginator = Paginator(Sticker.objects.filter(owner=site_owner), S_A_O_P)
        else:
            paginator = Paginator(Sticker.objects.all(), S_A_O_P)
    try:
        stickers = paginator.page(page)
    except EmptyPage:
        raise APIException("This page is empty", code=404)
    return Response({'stickers': stickers}, template_name='stickers/stickers_for_placer_render.html')


class CustomAuthToken(ObtainAuthToken):
    """
    Token for accessing PlacerApiView and sticker api
    """

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        if user.groups.filter(name='site_owners').exists():
            token, created = Token.objects.get_or_create(user=user)
            return Response({
                'token': token.key,
                'site_name': user.profile.site_name,
            })
        else:
            return Response({
                'error': _('You are not site owner')
            })

