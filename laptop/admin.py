from django.contrib import admin

# Register your models here.
from laptop.models import Laptop

admin.site.register(Laptop)
