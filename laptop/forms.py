from django import forms
from django.utils.translation import gettext_lazy as _

from laptop.models import Laptop


class LaptopForm(forms.ModelForm):
    class Meta:
        model = Laptop
        fields = ('manufacturer', 'model', 'width', 'height', 'image')
        labels = {
            'manufacturer': _('Manufacturer'),
            'model': _('Model'),
            'width': _('Backside width'),
            'height': _('Backside height'),
            'image': _('Backside image'),
        }
