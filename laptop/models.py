from django.core.validators import MinValueValidator
from django.db import models

# Create your models here.
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from django.utils.text import slugify
from text_unidecode import unidecode


def laptop_image_directory_path(instance, filename):
    return 'laptops/{0}/{1}.{2}'.format(instance.manufacturer, instance.model, filename.split('.')[-1])


class Laptop(models.Model):
    manufacturer = models.CharField(max_length=30)
    model = models.CharField(max_length=50)
    slug = models.SlugField()
    height = models.FloatField(validators=[MinValueValidator(0.0)])
    width = models.FloatField(validators=[MinValueValidator(0.0)])
    image = models.ImageField(upload_to=laptop_image_directory_path)

    def __str__(self):
        return '{} {}'.format(self.manufacturer, self.model)

    def save(self, **kwargs):
        if not self.id:  # if this is a new item
            newslug = '{0} {1}'.format(self.manufacturer, self.model)
            self.slug = slugify(unidecode(newslug))
        super(Laptop, self).save(**kwargs)


@receiver(pre_delete, sender=Laptop)
def image_path_delete(sender, instance, **kwargs):
    storage, path = instance.image.storage, instance.image.path
    storage.delete(path)
