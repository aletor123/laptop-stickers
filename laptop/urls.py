from django.urls import path

from laptop.views import LaptopList

app_name = 'laptop'

urlpatterns = [
    path('', LaptopList.as_view(), name='laptops')
]
