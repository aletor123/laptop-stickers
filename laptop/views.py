from django.shortcuts import render

# Create your views here.
from django.views.generic import ListView
from django.views.generic.edit import FormMixin

from laptop.forms import LaptopForm
from laptop.models import Laptop


class LaptopList(FormMixin, ListView):
    model = Laptop
    object_list = Laptop.objects.all()
    context_object_name = 'laptops'
    template_name = 'laptop/laptop_list.html'
    success_url = '/laptop/'
    form_class = LaptopForm
    paginate_by = 25

    def post(self, request, *args, **kwargs):
        form = LaptopForm(request.POST, request.FILES)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_queryset(self):
        return Laptop.objects.all()

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)
