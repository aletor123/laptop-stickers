from django.contrib import admin

# Register your models here.
from stickers.models import Sticker

admin.site.register(Sticker)
