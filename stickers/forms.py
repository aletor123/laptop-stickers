from django import forms
from django.utils.translation import gettext_lazy as _

from stickers.models import Sticker


class StickerForm(forms.ModelForm):
    class Meta:
        model = Sticker
        exclude = ['owner']
        labels = {
            'name': _('Name'),
            'width': _('Width'),
            'height': _('Height'),
            'image': _('Sticker'),
        }
        help_texts = {
            'image': _('use PNG'),
            'width': _('mm'),
            'height': _('mm'),
        }
