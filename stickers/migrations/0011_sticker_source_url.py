# Generated by Django 3.0.8 on 2020-09-13 12:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('stickers', '0010_auto_20200819_0108'),
    ]

    operations = [
        migrations.AddField(
            model_name='sticker',
            name='source_url',
            field=models.URLField(blank=True, null=True, verbose_name='Url of sticker on owner site'),
        ),
    ]
