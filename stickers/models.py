from django.contrib.auth.models import User
from django.core.validators import MinValueValidator
from django.db import models
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from taggit.managers import TaggableManager
from django.utils.translation import gettext_lazy as _


# Create your models here.

def user_directory_path(instance, filename):
    return 'stickers/{0}/{1}.{2}'.format(instance.owner.username, instance.name, filename.split('.')[-1])


class Sticker(models.Model):
    name = models.CharField(max_length=50)
    height = models.FloatField(validators=[MinValueValidator(0.0)])
    width = models.FloatField(validators=[MinValueValidator(0.0)])
    image = models.ImageField(upload_to=user_directory_path)
    tags = TaggableManager()
    owner = models.ForeignKey(User, related_name='stickers', on_delete=models.CASCADE)
    source_url = models.URLField(verbose_name=_('Url of sticker on owner site'), blank=True, null=True)

    class Meta:
        ordering = ['-name']
        unique_together = ('name', 'owner')

    def tags_list(self):
        return [tag.name for tag in self.tags.all()]

    def __str__(self):
        return '{}'.format(self.name)


@receiver(pre_delete, sender=Sticker)
def image_path_delete(sender, instance, **kwargs):
    storage, path = instance.image.storage, instance.image.path
    storage.delete(path)
