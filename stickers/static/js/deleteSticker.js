function deleteStickerFromDB(button) {
    let sticker_id = button.getAttribute('data-sticker');
    $.ajax({
            url: '/api/stickers/' + sticker_id + '/',
            method: 'DELETE',
            success: function (sticker) {
                $('#sticker_block_'+sticker_id).remove();
            },
            error: function (d) {
                console.log(d);
            }
    });
}