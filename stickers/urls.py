from django.urls import path

from laptop.views import LaptopList
from stickers.views import StickerList

app_name = 'sticker'

urlpatterns = [
    path('', StickerList.as_view(), name='stickers')
]
