from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404
from django.utils.translation import gettext as _

# Create your views here.
from django.views.generic import ListView
from django.views.generic.edit import FormMixin

from stickers.forms import StickerForm
from stickers.models import Sticker


class StickerList(LoginRequiredMixin, FormMixin, ListView):
    model = Sticker
    context_object_name = 'stickers'
    template_name = '/stickers/sticker_list.html'
    success_url = '/sticker/'
    form_class = StickerForm
    paginate_by = 25

    def get_queryset(self):
        return Sticker.objects.filter(owner=self.request.user)

    def post(self, request, *args, **kwargs):
        form = StickerForm(request.POST, request.FILES)
        if form.is_valid():
            sticker = form.save(commit=False)
            sticker.owner = request.user
            sticker.save()
            form.save_m2m()
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        return super().form_valid(form)
