from django import forms
from django.utils.translation import gettext_lazy as _


class ChoosingForm(forms.Form):
    manufacturer = forms.ChoiceField()
    model = forms.ChoiceField()

    def __init__(self, *args, manufacturer_choices, model_choices, **kwargs):
        super(ChoosingForm, self).__init__(*args, **kwargs)
        self.fields['manufacturer'].choices = manufacturer_choices
        self.fields['model'].choices = model_choices
