$.event.special.widthChanged = {
    remove: function() {
        $(this).children('iframe.width-changed').remove();
    },
    add: function () {
        let elm = $(this);
        let iframe = elm.children('iframe.width-changed');
        if (!iframe.length) {
            iframe = $('<iframe/>').addClass('width-changed').prependTo(this);
        }
        let oldWidth = elm.width();

        function elmResized() {
            const width = elm.width();
            if (oldWidth != width) {
                elm.trigger('widthChanged', [width, oldWidth]);
                oldWidth = width;
            }
        }

        let timer = 0;
        const ielm = iframe[0];
        (ielm.contentWindow || ielm).onresize = function() {
            clearTimeout(timer);
            timer = setTimeout(elmResized, 20);
        };
    }
}

$('#laptopStickers #workArea').on('widthChanged',function(){
	$('#laptopStickers .sticker_div').each(function (e) {
	    let containment = calculate_pos($(this).find('.sticker_div_rotatable'));
	    $(this).draggable('option', 'containment', containment);
	    let pos = $(this).position();
	    let work_area = $('#laptopStickers #workArea');
	    let work_area_offset = work_area.offset();
	    // считаем containment относительно workArea
        // 0,1 - левый верхний угол
        // 2,3 - правый нижний угол
        let containment_rel = new Array(4)
        containment_rel[0] = containment[0] - work_area_offset['left'];
        containment_rel[1] = containment[1] - work_area_offset['top'];
        containment_rel[2] = containment[2] - work_area_offset['left'];
        containment_rel[3] = containment[3] - work_area_offset['top'];

	    // если выходит за границы, то перемещаем на край
        if (pos['left'] > containment_rel[2]){
            $(this).css('left', containment_rel[2]);
        } else if (pos['left'] < containment_rel[0]){
            $(this).css('left', containment_rel[0]);
        }
        if (pos['top'] > containment_rel[3]){
            $(this).css('top', containment_rel[3]);
        } else if (pos['top'] < containment_rel[1]){
            $(this).css('top', containment_rel[1]);
        }
    });
});