function calculate_pos(div) {
    let l1, l2, t1, t2;

    let work_area = $('#laptopStickers #workArea');
    let work_area_offset = work_area.offset();

    // Начальные границы(на 1/3 картинка может выйти за рамки)
    l1 = work_area_offset['left'] - div.width()*(1/3);
    l2 = work_area_offset['left'] + work_area.width() - div.width()*(2/3);
    t1 = work_area_offset['top'] - div.height()*(1/3);
    t2 = work_area_offset['top'] + work_area.height() - div.height() *(2/3);
    return [l1, t1, l2, t2]
}



function end_rotating(div_rotatable, event, ui) {
    let l1, l2, t1, t2;
    // Разница является просто позицией rot_dit относительно drag_div
    let pos_sub = div_rotatable.position();

    let containment = calculate_pos(div_rotatable);



    if (pos_sub['top'] < 0) {
        containment[1] = containment[1] - pos_sub['top'] * 0.3;
        containment[3] = containment[3] - pos_sub['top'] * 0.3;
    }
    div_rotatable.parent().draggable('option', 'containment', containment);
}