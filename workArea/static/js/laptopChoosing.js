function activate() {
            $('#laptopStickers #stickers_block').show();
            $('#laptopStickers #search_by_tags_button').removeAttr('disabled');
}
function activateStickers(laptop_image) {
    if (laptop_image.get(0).complete || laptop_image.get(0).readyState === 4){
        activate();
    }else {
        laptop_image.on('load', function () {
            activate();
        })
    }
}

//Выбор производителя, в модели будут теперь только ноутбуки от этого производителя
$("#laptopStickers #laptopChoosingForm select").eq(0).change(function(){
    $.ajax({
            url: stickers_site_url + '/api/chooseLaptopManufacturer/',
            method: 'GET',
            data: {'manufacturer': $(this).val()},
            success: function (model_choices) {
                let model_select = $("#laptopStickers #laptopChoosingForm select").eq(1);
                model_select.empty();
                model_select.append(`<option value="none">${model_choices["none"]}</option>`);
                for (let key in model_choices){
                    if (key !== 'none') {
                        model_select.append(`<option value="${key}">${model_choices[key]}</option>`);
                    }
                }
            },
            error: function (d) {
                console.log(d);
            }
    });
});
// выбор модели и вывод на workArea
$("#laptopStickers #laptopChoosingForm select").eq(1).change(function(){
        if ($(this).val() === 'none'){return null}
        $.ajax({
            url: stickers_site_url + '/api/chooseLaptop/',
            method: 'GET',
            data: {'id':$(this).val()},
            success: function (laptop) {
                let laptop_image = $('#laptopStickers #laptopImage');
                laptop_image.attr('src', laptop['image']);
                laptop_image.data('w', laptop['width']);
                laptop_image.data('h', laptop['height']);
                $('#laptopStickers #laptopSize').html(laptop['width'] + 'x' + laptop['height'] + 'mm');
                activateStickers(laptop_image);
            },
            error: function (d) {
                console.log(d);
            }
        });
});

$('#laptopStickers #choose_laptop').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: stickers_site_url + '/api/chooseLaptop/',
            method: 'POST',
            data: $('#laptopStickers #laptopChoosingForm').serialize(),
            success: function (laptop_data) {
                let laptop = JSON.parse(laptop_data);
                let laptop_image = $('#laptopStickers #laptopImage');
                laptop_image.attr('src', stickers_site_url + laptop['image.url']);
                laptop_image.data('w', laptop['width']);
                laptop_image.data('h', laptop['height']);
                activateStickers(laptop_image);
            },
            error: function (d) {
                console.log(d);
            }
        });
})