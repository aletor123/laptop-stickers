$('#laptopStickers #search_by_tags_button').on('click', function (e) {
    e.preventDefault();
    let url = stickers_site_url + '/api/searchByTags/';
    let data = $('#laptopStickers #sticker_search').serialize();
    // если поле пустое то начать выводить все постранично
    if ($('#laptopStickers #sticker_search #id_tag').val() === ''){
        url = stickers_site_url + '/api/uploadSticker/';
        data = {"page":1,
                "site_name": $('#laptopStickers #id_site_name').val(),
            };
        update_upload_variables();
    }
    $.ajax({
            url: url,
            method: 'GET',
            data: data,
            success: function (stickers_html) {
                $('#laptopStickers #stickers_ids').remove();
                $('#laptopStickers #stickers_block').html(stickers_html);
                update_upload_variables();
            },
            error: function (d) {
                console.log(d);
            }
        });
});