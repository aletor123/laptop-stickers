let num_elem = 0;

function init_drag(sticker_div){
	sticker_div.draggable({
		cursor: 'move', // вид курсора
		containment: calculate_pos(sticker_div), // ограничение перемещения
		scroll: false, // автоскроллинг
		drag: false, // событие при перемещении
        stack: '.sticker_div',
	});
    // TODO сделать чтобы при нажатии стикер был поверх других
	sticker_div.on('click', function () {
            $(this).css('z-index', 100);
        }
    );
}


function init_rotatable (sticker_div_rotatable) {
    sticker_div_rotatable.rotatable({
        stop: function (event, ui) {
            end_rotating(sticker_div_rotatable, event, ui)
        },
    });
}

function deleteSticker(num_el) {
    $('#laptopStickers #sticker_div_'+num_el).remove();
    num_elem --;
}

function init_buttons(sticker_div_rotatable) {
    let close_span = sticker_div_rotatable.find('span');
    let rotate_button = sticker_div_rotatable.find('.ui-rotatable-handle');
    // считаем позицию кнопки вращения
    rotate_button.css({
        'position': 'absolute',
        'top': '90%',
    });
    // показываем и скрываем кнопки
    sticker_div_rotatable.on("mouseover", function (e) {
        close_span.css('display', 'block');
        rotate_button.css('display', 'block');
    });
    sticker_div_rotatable.on("mouseout", function (e) {
        close_span.css('display', 'none');
        rotate_button.css('display', 'none');
    });
}

function addSticker(button) {
    let url = button.getAttribute("data-url");
    // получаем размеры стикера и ноутбука
    let w = button.getAttribute("data-w");
    let l_w = $('#laptopStickers #laptopImage').data('w');
    // вычисляем отношение
    let ratio_w = w/l_w * 100;
	// добавляем див для перемещения в рабочую область
    let sticker_div_var = '<div class="sticker_div" id="sticker_div_' + num_elem + '"></div>';
    //добавляем див для вращения в див для перемещения
    let sticker_div_rotatable_var = '<div class="sticker_div_rotatable" id="sticker_div_rotatable_' + num_elem + '">' +
        '<span onclick="deleteSticker('+num_elem+')" class="remove_sticker" >X</span>' +
        '</div>';
    $('#laptopStickers #workArea').append(sticker_div_var);
	// задаем место появления в рабочей области
    let sticker_div = $('#laptopStickers #sticker_div_'+num_elem);
	sticker_div.css({
        'position': 'absolute',
		'top': '0px',
		'left': '0px'
	});
	// добавляем див вращения с картинкой
    sticker_div.append(sticker_div_rotatable_var);
    let sticker_div_rotatable = $('#laptopStickers #sticker_div_rotatable_'+num_elem);
	sticker_div_rotatable.append('<img id="sticker_image_'+ num_elem +'">'
    );
	sticker_div.css({
        width: ratio_w+'%',
    });


	let sticker_image = $('#laptopStickers #sticker_image_'+num_elem);
	sticker_image.attr('src', stickers_site_url + url);
    sticker_image.addClass('sticker_image');
    if (sticker_image.get(0).complete || sticker_image.get(0).readyState === 4){
        init_drag(sticker_div);
    }else {
        sticker_image.on('load', function () {
            init_drag(sticker_div);
        })
    }
	init_rotatable(sticker_div_rotatable);
	init_buttons(sticker_div_rotatable);
	num_elem ++;
}