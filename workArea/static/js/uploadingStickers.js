let page = 1;
let empty_page = false;
let block_request = false;

let stickersBlock = $('#laptopStickers #stickers_block');

let scrollHeight = document.getElementById("stickers_block")

function update_upload_variables() {
    page = 1;
    empty_page = false;
    block_request = false;
}

function get_stickers_ids() {
    return $('#laptopStickers #stickers_ids').data('stickers_ids')
}

function get_site_name() {
    return $('#laptopStickers #id_site_name').val()
}

$("#stickers_block").scroll(function() {
    // при прокручивании стикеры будут подгружаться по 8 штук за раз
    const margin = scrollHeight.scrollHeight - stickersBlock.height() - 100;
    if (stickersBlock.scrollTop() > margin && empty_page === false && block_request === false) {
        block_request = true;
        page += 1;
        $.ajax({
            url: stickers_site_url + '/api/uploadSticker/',
            method: 'GET',
            contentType: 'application/json',
            data: {
                "page":page,
                'stickers_ids': get_stickers_ids(),
                'site_name': get_site_name()},
            success: function (sticker_html) {
                if(sticker_html !== ''){
                    stickersBlock.append(sticker_html);
                    block_request = false;
                }else {
                    empty_page = true;
                }
            },
            error: function (d) {
                console.log(d);
            }
        });
    }
});