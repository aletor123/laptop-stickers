from django.urls import path

from workArea.views import Placer

app_name = 'workArea'

urlpatterns = [
    path('', Placer.as_view(), name='workArea'),
]
