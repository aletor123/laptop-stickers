from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView
from django.views.generic.edit import FormMixin

from laptop.models import Laptop
from workArea.forms import ChoosingForm
from stickers.models import Sticker

STICKERS_AT_ONE_PAGE = 8


def get_context():
    laptops = Laptop.objects.all()
    model_choices = [(laptop.id, laptop.model) for laptop in laptops]
    model_choices.insert(0, ('0', _('--Choice model--')))
    uniq_manufacturers = []
    for laptop in laptops:
        if laptop.manufacturer in uniq_manufacturers:
            continue
        uniq_manufacturers.append(laptop.manufacturer)
    manufacturer_choices = [(manufacturer, manufacturer) for manufacturer in uniq_manufacturers]
    manufacturer_choices.insert(0, ('0', _('--Choice manufacturer--')))
    return ChoosingForm(manufacturer_choices=manufacturer_choices, model_choices=model_choices)


class Placer(TemplateView):
    template_name = 'placer/placer.html'

    def get_context_data(self, **kwargs):
        context = super(Placer, self).get_context_data(**kwargs)
        context['stickers'] = Sticker.objects.all()[:STICKERS_AT_ONE_PAGE]
        context['laptopForm'] = get_context()
        return context

# TODO добавить кнопку "показать только свои стикеры"

# TODO добавить возможность добавлять свои стикеры без регистрации
#  ,но чтобы после окончания сессии они удалялись

# TODO добавить возможность добавлять свою фотку ноутбука
